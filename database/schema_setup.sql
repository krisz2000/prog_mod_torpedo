--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: torpedo; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA torpedo;


ALTER SCHEMA torpedo OWNER TO postgres;

--
-- Name: ship; Type: TYPE; Schema: torpedo; Owner: postgres
--

CREATE TYPE torpedo.ship AS (
	"VerPosition" smallint,
	"HorPosition" smallint,
	"Arrangement" character(1),
	"Length" smallint,
	"Owner" character varying(6)
);


ALTER TYPE torpedo.ship OWNER TO postgres;

--
-- Name: create_hit(integer, smallint, smallint, character varying); Type: FUNCTION; Schema: torpedo; Owner: postgres
--

CREATE FUNCTION torpedo.create_hit("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Owner" character varying) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."HitTable" VALUES(DEFAULT, "SaveID", "VerPosition", "HorPosition", "Owner");$$;


ALTER FUNCTION torpedo.create_hit("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Owner" character varying) OWNER TO postgres;

--
-- Name: create_save(character varying); Type: FUNCTION; Schema: torpedo; Owner: postgres
--

CREATE FUNCTION torpedo.create_save("SaveName" character varying) RETURNS integer
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."Save" VALUES(DEFAULT, localtimestamp, "SaveName");

 SELECT lastval();$$;


ALTER FUNCTION torpedo.create_save("SaveName" character varying) OWNER TO postgres;

--
-- Name: create_ship(integer, smallint, smallint, character, smallint, character varying); Type: FUNCTION; Schema: torpedo; Owner: postgres
--

CREATE FUNCTION torpedo.create_ship("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."Ship" VALUES(DEFAULT, "SaveID", "VerPosition", "HorPosition", "Arrangement", "Length", "Owner");$$;


ALTER FUNCTION torpedo.create_ship("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying) OWNER TO postgres;

--
-- Name: delete_save(integer); Type: FUNCTION; Schema: torpedo; Owner: postgres
--

CREATE FUNCTION torpedo.delete_save("DeleteID" integer) RETURNS void
    LANGUAGE sql
    AS $$

DELETE FROM Torpedo."Ship" WHERE "Ship"."SaveID" = "DeleteID";

DELETE FROM Torpedo."HitTable" WHERE "HitTable"."SaveID" = "DeleteID";

DELETE FROM Torpedo."Save" WHERE "Save"."SaveID" = "DeleteID";

$$;


ALTER FUNCTION torpedo.delete_save("DeleteID" integer) OWNER TO postgres;

--
-- Name: get_hits(integer); Type: FUNCTION; Schema: torpedo; Owner: postgres
--

CREATE FUNCTION torpedo.get_hits("inputSaveID" integer) RETURNS TABLE("VerPosition" smallint, "HorPosition" smallint, "Owner" character varying)
    LANGUAGE sql
    AS $$SELECT "VerPosition", "HorPosition", "Owner" FROM Torpedo."HitTable" WHERE "HitTable"."SaveID" = "inputSaveID"$$;


ALTER FUNCTION torpedo.get_hits("inputSaveID" integer) OWNER TO postgres;

--
-- Name: get_saves(); Type: FUNCTION; Schema: torpedo; Owner: postgres
--

CREATE FUNCTION torpedo.get_saves() RETURNS TABLE(id integer, "SaveName" character varying, "CreatedAt" timestamp without time zone)
    LANGUAGE sql
    AS $$SELECT "SaveID", "SaveName", date_trunc('second', "CreatedAt") FROM Torpedo."Save" ORDER BY "CreatedAt" DESC$$;


ALTER FUNCTION torpedo.get_saves() OWNER TO postgres;

--
-- Name: get_ships(integer); Type: FUNCTION; Schema: torpedo; Owner: postgres
--

CREATE FUNCTION torpedo.get_ships("inputSaveID" integer) RETURNS TABLE("VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying)
    LANGUAGE sql
    AS $$SELECT "VerPosition", "HorPosition", "Arrangement", "Length", "Owner" FROM Torpedo."Ship" WHERE "Ship"."SaveID" = "inputSaveID"$$;


ALTER FUNCTION torpedo.get_ships("inputSaveID" integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: HitTable; Type: TABLE; Schema: torpedo; Owner: postgres
--

CREATE TABLE torpedo."HitTable" (
    "HitTableID" integer NOT NULL,
    "SaveID" integer,
    "VerPosition" smallint,
    "HorPosition" smallint,
    "Owner" character varying(6)
);


ALTER TABLE torpedo."HitTable" OWNER TO postgres;

--
-- Name: HitTable_HitTableID_seq; Type: SEQUENCE; Schema: torpedo; Owner: postgres
--

CREATE SEQUENCE torpedo."HitTable_HitTableID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE torpedo."HitTable_HitTableID_seq" OWNER TO postgres;

--
-- Name: HitTable_HitTableID_seq; Type: SEQUENCE OWNED BY; Schema: torpedo; Owner: postgres
--

ALTER SEQUENCE torpedo."HitTable_HitTableID_seq" OWNED BY torpedo."HitTable"."HitTableID";


--
-- Name: Save; Type: TABLE; Schema: torpedo; Owner: postgres
--

CREATE TABLE torpedo."Save" (
    "SaveID" integer NOT NULL,
    "CreatedAt" timestamp without time zone,
    "SaveName" character varying(30)
);


ALTER TABLE torpedo."Save" OWNER TO postgres;

--
-- Name: Save_SaveID_seq; Type: SEQUENCE; Schema: torpedo; Owner: postgres
--

CREATE SEQUENCE torpedo."Save_SaveID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE torpedo."Save_SaveID_seq" OWNER TO postgres;

--
-- Name: Save_SaveID_seq; Type: SEQUENCE OWNED BY; Schema: torpedo; Owner: postgres
--

ALTER SEQUENCE torpedo."Save_SaveID_seq" OWNED BY torpedo."Save"."SaveID";


--
-- Name: Ship; Type: TABLE; Schema: torpedo; Owner: postgres
--

CREATE TABLE torpedo."Ship" (
    "ShipID" integer NOT NULL,
    "SaveID" integer,
    "VerPosition" smallint,
    "HorPosition" smallint,
    "Arrangement" character(1),
    "Length" smallint,
    "Owner" character varying(6)
);


ALTER TABLE torpedo."Ship" OWNER TO postgres;

--
-- Name: Ship_ShipID_seq; Type: SEQUENCE; Schema: torpedo; Owner: postgres
--

CREATE SEQUENCE torpedo."Ship_ShipID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE torpedo."Ship_ShipID_seq" OWNER TO postgres;

--
-- Name: Ship_ShipID_seq; Type: SEQUENCE OWNED BY; Schema: torpedo; Owner: postgres
--

ALTER SEQUENCE torpedo."Ship_ShipID_seq" OWNED BY torpedo."Ship"."ShipID";


--
-- Name: HitTable HitTableID; Type: DEFAULT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."HitTable" ALTER COLUMN "HitTableID" SET DEFAULT nextval('torpedo."HitTable_HitTableID_seq"'::regclass);


--
-- Name: Save SaveID; Type: DEFAULT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."Save" ALTER COLUMN "SaveID" SET DEFAULT nextval('torpedo."Save_SaveID_seq"'::regclass);


--
-- Name: Ship ShipID; Type: DEFAULT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."Ship" ALTER COLUMN "ShipID" SET DEFAULT nextval('torpedo."Ship_ShipID_seq"'::regclass);


--
-- Name: HitTable HitTable_pkey; Type: CONSTRAINT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."HitTable"
    ADD CONSTRAINT "HitTable_pkey" PRIMARY KEY ("HitTableID");


--
-- Name: Save Save_pkey; Type: CONSTRAINT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."Save"
    ADD CONSTRAINT "Save_pkey" PRIMARY KEY ("SaveID");


--
-- Name: Ship Ship_pkey; Type: CONSTRAINT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."Ship"
    ADD CONSTRAINT "Ship_pkey" PRIMARY KEY ("ShipID");


--
-- Name: HitTable HitTable_SaveID_fkey; Type: FK CONSTRAINT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."HitTable"
    ADD CONSTRAINT "HitTable_SaveID_fkey" FOREIGN KEY ("SaveID") REFERENCES torpedo."Save"("SaveID");


--
-- Name: Ship Ship_SaveID_fkey; Type: FK CONSTRAINT; Schema: torpedo; Owner: postgres
--

ALTER TABLE ONLY torpedo."Ship"
    ADD CONSTRAINT "Ship_SaveID_fkey" FOREIGN KEY ("SaveID") REFERENCES torpedo."Save"("SaveID");


--
-- PostgreSQL database dump complete
--

