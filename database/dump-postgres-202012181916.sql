PGDMP     3                    x            postgres    13.1    13.1 %    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    13442    postgres    DATABASE     m   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United Kingdom.1250';
    DROP DATABASE postgres;
                postgres    false            �           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3027                        2615    16521    torpedo    SCHEMA        CREATE SCHEMA torpedo;
    DROP SCHEMA torpedo;
                postgres    false            �           1247    16577    ship    TYPE     �   CREATE TYPE torpedo.ship AS (
	"VerPosition" smallint,
	"HorPosition" smallint,
	"Arrangement" character(1),
	"Length" smallint,
	"Owner" character varying(6)
);
    DROP TYPE torpedo.ship;
       torpedo          postgres    false    6            �            1255    16560 :   create_hit(integer, smallint, smallint, character varying)    FUNCTION       CREATE FUNCTION torpedo.create_hit("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Owner" character varying) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."HitTable" VALUES(DEFAULT, "SaveID", "VerPosition", "HorPosition", "Owner");$$;
    DROP FUNCTION torpedo.create_hit("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Owner" character varying);
       torpedo          postgres    false    6            �            1255    16595    create_save(character varying)    FUNCTION     �   CREATE FUNCTION torpedo.create_save("SaveName" character varying) RETURNS integer
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."Save" VALUES(DEFAULT, localtimestamp, "SaveName");
 SELECT lastval();$$;
 A   DROP FUNCTION torpedo.create_save("SaveName" character varying);
       torpedo          postgres    false    6            �            1255    16565 P   create_ship(integer, smallint, smallint, character, smallint, character varying)    FUNCTION     M  CREATE FUNCTION torpedo.create_ship("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."Ship" VALUES(DEFAULT, "SaveID", "VerPosition", "HorPosition", "Arrangement", "Length", "Owner");$$;
 �   DROP FUNCTION torpedo.create_ship("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying);
       torpedo          postgres    false    6            �            1255    16574    delete_save(integer)    FUNCTION     %  CREATE FUNCTION torpedo.delete_save("SaveID" integer) RETURNS void
    LANGUAGE sql
    AS $$
DELETE FROM Torpedo."Ship" WHERE "Ship"."SaveID" = "SaveID";
DELETE FROM Torpedo."HitTable" WHERE "HitTable"."SaveID" = "SaveID";
DELETE FROM Torpedo."Save" WHERE "Save"."SaveID" = "SaveID";
$$;
 5   DROP FUNCTION torpedo.delete_save("SaveID" integer);
       torpedo          postgres    false    6            �            1255    16942    get_hits(integer)    FUNCTION       CREATE FUNCTION torpedo.get_hits("inputSaveID" integer) RETURNS TABLE("VerPosition" smallint, "HorPosition" smallint, "Owner" character varying)
    LANGUAGE sql
    AS $$SELECT "VerPosition", "HorPosition", "Owner" FROM Torpedo."HitTable" WHERE "HitTable"."SaveID" = "inputSaveID"$$;
 7   DROP FUNCTION torpedo.get_hits("inputSaveID" integer);
       torpedo          postgres    false    6            �            1255    16940    get_saves()    FUNCTION       CREATE FUNCTION torpedo.get_saves() RETURNS TABLE(id integer, "SaveName" character varying, "CreatedAt" timestamp without time zone)
    LANGUAGE sql
    AS $$SELECT "SaveID", "SaveName", date_trunc('second', "CreatedAt") FROM Torpedo."Save" ORDER BY "CreatedAt" DESC$$;
 #   DROP FUNCTION torpedo.get_saves();
       torpedo          postgres    false    6            �            1255    16944    get_ships(integer)    FUNCTION     [  CREATE FUNCTION torpedo.get_ships("inputSaveID" integer) RETURNS TABLE("VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying)
    LANGUAGE sql
    AS $$SELECT "VerPosition", "HorPosition", "Arrangement", "Length", "Owner" FROM Torpedo."Ship" WHERE "Ship"."SaveID" = "inputSaveID"$$;
 8   DROP FUNCTION torpedo.get_ships("inputSaveID" integer);
       torpedo          postgres    false    6            �            1259    16524    HitTable    TABLE     �   CREATE TABLE torpedo."HitTable" (
    "HitTableID" integer NOT NULL,
    "SaveID" integer,
    "VerPosition" smallint,
    "HorPosition" smallint,
    "Owner" character varying(6)
);
    DROP TABLE torpedo."HitTable";
       torpedo         heap    postgres    false    6            �            1259    16522    HitTable_HitTableID_seq    SEQUENCE     �   CREATE SEQUENCE torpedo."HitTable_HitTableID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE torpedo."HitTable_HitTableID_seq";
       torpedo          postgres    false    203    6            �           0    0    HitTable_HitTableID_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE torpedo."HitTable_HitTableID_seq" OWNED BY torpedo."HitTable"."HitTableID";
          torpedo          postgres    false    202            �            1259    16532    Save    TABLE     �   CREATE TABLE torpedo."Save" (
    "SaveID" integer NOT NULL,
    "CreatedAt" timestamp without time zone,
    "SaveName" character varying(30)
);
    DROP TABLE torpedo."Save";
       torpedo         heap    postgres    false    6            �            1259    16530    Save_SaveID_seq    SEQUENCE     �   CREATE SEQUENCE torpedo."Save_SaveID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE torpedo."Save_SaveID_seq";
       torpedo          postgres    false    205    6            �           0    0    Save_SaveID_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE torpedo."Save_SaveID_seq" OWNED BY torpedo."Save"."SaveID";
          torpedo          postgres    false    204            �            1259    16540    Ship    TABLE     �   CREATE TABLE torpedo."Ship" (
    "ShipID" integer NOT NULL,
    "SaveID" integer,
    "VerPosition" smallint,
    "HorPosition" smallint,
    "Arrangement" character(1),
    "Length" smallint,
    "Owner" character varying(6)
);
    DROP TABLE torpedo."Ship";
       torpedo         heap    postgres    false    6            �            1259    16538    Ship_ShipID_seq    SEQUENCE     �   CREATE SEQUENCE torpedo."Ship_ShipID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE torpedo."Ship_ShipID_seq";
       torpedo          postgres    false    6    207            �           0    0    Ship_ShipID_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE torpedo."Ship_ShipID_seq" OWNED BY torpedo."Ship"."ShipID";
          torpedo          postgres    false    206            ;           2604    16527    HitTable HitTableID    DEFAULT     �   ALTER TABLE ONLY torpedo."HitTable" ALTER COLUMN "HitTableID" SET DEFAULT nextval('torpedo."HitTable_HitTableID_seq"'::regclass);
 G   ALTER TABLE torpedo."HitTable" ALTER COLUMN "HitTableID" DROP DEFAULT;
       torpedo          postgres    false    202    203    203            <           2604    16535    Save SaveID    DEFAULT     r   ALTER TABLE ONLY torpedo."Save" ALTER COLUMN "SaveID" SET DEFAULT nextval('torpedo."Save_SaveID_seq"'::regclass);
 ?   ALTER TABLE torpedo."Save" ALTER COLUMN "SaveID" DROP DEFAULT;
       torpedo          postgres    false    205    204    205            =           2604    16543    Ship ShipID    DEFAULT     r   ALTER TABLE ONLY torpedo."Ship" ALTER COLUMN "ShipID" SET DEFAULT nextval('torpedo."Ship_ShipID_seq"'::regclass);
 ?   ALTER TABLE torpedo."Ship" ALTER COLUMN "ShipID" DROP DEFAULT;
       torpedo          postgres    false    206    207    207            �          0    16524    HitTable 
   TABLE DATA           d   COPY torpedo."HitTable" ("HitTableID", "SaveID", "VerPosition", "HorPosition", "Owner") FROM stdin;
    torpedo          postgres    false    203            �          0    16532    Save 
   TABLE DATA           D   COPY torpedo."Save" ("SaveID", "CreatedAt", "SaveName") FROM stdin;
    torpedo          postgres    false    205            �          0    16540    Ship 
   TABLE DATA           u   COPY torpedo."Ship" ("ShipID", "SaveID", "VerPosition", "HorPosition", "Arrangement", "Length", "Owner") FROM stdin;
    torpedo          postgres    false    207            �           0    0    HitTable_HitTableID_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('torpedo."HitTable_HitTableID_seq"', 30, true);
          torpedo          postgres    false    202            �           0    0    Save_SaveID_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('torpedo."Save_SaveID_seq"', 6, true);
          torpedo          postgres    false    204            �           0    0    Ship_ShipID_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('torpedo."Ship_ShipID_seq"', 108, true);
          torpedo          postgres    false    206            ?           2606    16529    HitTable HitTable_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY torpedo."HitTable"
    ADD CONSTRAINT "HitTable_pkey" PRIMARY KEY ("HitTableID");
 E   ALTER TABLE ONLY torpedo."HitTable" DROP CONSTRAINT "HitTable_pkey";
       torpedo            postgres    false    203            A           2606    16537    Save Save_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY torpedo."Save"
    ADD CONSTRAINT "Save_pkey" PRIMARY KEY ("SaveID");
 =   ALTER TABLE ONLY torpedo."Save" DROP CONSTRAINT "Save_pkey";
       torpedo            postgres    false    205            C           2606    16545    Ship Ship_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY torpedo."Ship"
    ADD CONSTRAINT "Ship_pkey" PRIMARY KEY ("ShipID");
 =   ALTER TABLE ONLY torpedo."Ship" DROP CONSTRAINT "Ship_pkey";
       torpedo            postgres    false    207            D           2606    16546    HitTable HitTable_SaveID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY torpedo."HitTable"
    ADD CONSTRAINT "HitTable_SaveID_fkey" FOREIGN KEY ("SaveID") REFERENCES torpedo."Save"("SaveID");
 L   ALTER TABLE ONLY torpedo."HitTable" DROP CONSTRAINT "HitTable_SaveID_fkey";
       torpedo          postgres    false    205    2881    203            E           2606    16551    Ship Ship_SaveID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY torpedo."Ship"
    ADD CONSTRAINT "Ship_SaveID_fkey" FOREIGN KEY ("SaveID") REFERENCES torpedo."Save"("SaveID");
 D   ALTER TABLE ONLY torpedo."Ship" DROP CONSTRAINT "Ship_SaveID_fkey";
       torpedo          postgres    false    2881    205    207            �   �   x�M�MB!���0�(p7��3�7�ޡ��!�/3�cbJ�~�~�O`�ꘀ����Nݨ�������t1�m���h��l�f��NH�G�ޚG}�{q1��
-޶B�w�5�S2��#xl�<`�̽%�rJfp�w.ЫR��J�j=�hf��g3E�e^/!�?PN[�      �   q   x�]���0г���1�,�DJ'H�W����CSQ٠�Qng�0h���M�Qbe��C$��?F�<xX���P������@�v\'�F!�=_?i,A��Yb���������)�      �   K  x�e�A�!E��a"0`�]��H�f)Q���
lO4��7.�y����k�#���Ƿ��� /�����#qzO���P]��/��뢱'Z���&z]4��G%#�@ַ�^E�·��z�;�^�a�:@6��j>��Ӏ@�3�3XPF�VVͻc�:򆨮�DR[l<9뉞�]I�0�X�'����CĖIZ����;�g 
���t�>���tuםZ�����F�ټB9�Uk8I�y�̋u�̕�
���ЯVQ�5��xN\�M�1ƺte���#�	�r@��lw�g(w�h��#��j2����c��ev2��̄��*W�����8
�~-��1/��\q\�j�kx�L&�&���mTy`:�5gA��dް�F�N�u7����� ,kv�>��e�y��q�%0Ƴ�c���!���D�0��Ӿm�-��V�w��0�RخHixƁ�{�d
�L��ŀ a��dG6�S�2���� sJ��Y1oA�lx6Ȝ�dNF]�9��S�95�'�%g,b6K.��~q�����В�td�GA�i�;:;���կ_^��?�W�Z      %    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    13442    postgres    DATABASE     m   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United Kingdom.1250';
    DROP DATABASE postgres;
                postgres    false            �           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3027                        2615    16521    torpedo    SCHEMA        CREATE SCHEMA torpedo;
    DROP SCHEMA torpedo;
                postgres    false            �           1247    16577    ship    TYPE     �   CREATE TYPE torpedo.ship AS (
	"VerPosition" smallint,
	"HorPosition" smallint,
	"Arrangement" character(1),
	"Length" smallint,
	"Owner" character varying(6)
);
    DROP TYPE torpedo.ship;
       torpedo          postgres    false    6            �            1255    16560 :   create_hit(integer, smallint, smallint, character varying)    FUNCTION       CREATE FUNCTION torpedo.create_hit("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Owner" character varying) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."HitTable" VALUES(DEFAULT, "SaveID", "VerPosition", "HorPosition", "Owner");$$;
    DROP FUNCTION torpedo.create_hit("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Owner" character varying);
       torpedo          postgres    false    6            �            1255    16595    create_save(character varying)    FUNCTION     �   CREATE FUNCTION torpedo.create_save("SaveName" character varying) RETURNS integer
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."Save" VALUES(DEFAULT, localtimestamp, "SaveName");
 SELECT lastval();$$;
 A   DROP FUNCTION torpedo.create_save("SaveName" character varying);
       torpedo          postgres    false    6            �            1255    16565 P   create_ship(integer, smallint, smallint, character, smallint, character varying)    FUNCTION     M  CREATE FUNCTION torpedo.create_ship("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO Torpedo."Ship" VALUES(DEFAULT, "SaveID", "VerPosition", "HorPosition", "Arrangement", "Length", "Owner");$$;
 �   DROP FUNCTION torpedo.create_ship("SaveID" integer, "VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying);
       torpedo          postgres    false    6            �            1255    16574    delete_save(integer)    FUNCTION     %  CREATE FUNCTION torpedo.delete_save("SaveID" integer) RETURNS void
    LANGUAGE sql
    AS $$
DELETE FROM Torpedo."Ship" WHERE "Ship"."SaveID" = "SaveID";
DELETE FROM Torpedo."HitTable" WHERE "HitTable"."SaveID" = "SaveID";
DELETE FROM Torpedo."Save" WHERE "Save"."SaveID" = "SaveID";
$$;
 5   DROP FUNCTION torpedo.delete_save("SaveID" integer);
       torpedo          postgres    false    6            �            1255    16942    get_hits(integer)    FUNCTION       CREATE FUNCTION torpedo.get_hits("inputSaveID" integer) RETURNS TABLE("VerPosition" smallint, "HorPosition" smallint, "Owner" character varying)
    LANGUAGE sql
    AS $$SELECT "VerPosition", "HorPosition", "Owner" FROM Torpedo."HitTable" WHERE "HitTable"."SaveID" = "inputSaveID"$$;
 7   DROP FUNCTION torpedo.get_hits("inputSaveID" integer);
       torpedo          postgres    false    6            �            1255    16940    get_saves()    FUNCTION       CREATE FUNCTION torpedo.get_saves() RETURNS TABLE(id integer, "SaveName" character varying, "CreatedAt" timestamp without time zone)
    LANGUAGE sql
    AS $$SELECT "SaveID", "SaveName", date_trunc('second', "CreatedAt") FROM Torpedo."Save" ORDER BY "CreatedAt" DESC$$;
 #   DROP FUNCTION torpedo.get_saves();
       torpedo          postgres    false    6            �            1255    16944    get_ships(integer)    FUNCTION     [  CREATE FUNCTION torpedo.get_ships("inputSaveID" integer) RETURNS TABLE("VerPosition" smallint, "HorPosition" smallint, "Arrangement" character, "Length" smallint, "Owner" character varying)
    LANGUAGE sql
    AS $$SELECT "VerPosition", "HorPosition", "Arrangement", "Length", "Owner" FROM Torpedo."Ship" WHERE "Ship"."SaveID" = "inputSaveID"$$;
 8   DROP FUNCTION torpedo.get_ships("inputSaveID" integer);
       torpedo          postgres    false    6            �            1259    16524    HitTable    TABLE     �   CREATE TABLE torpedo."HitTable" (
    "HitTableID" integer NOT NULL,
    "SaveID" integer,
    "VerPosition" smallint,
    "HorPosition" smallint,
    "Owner" character varying(6)
);
    DROP TABLE torpedo."HitTable";
       torpedo         heap    postgres    false    6            �            1259    16522    HitTable_HitTableID_seq    SEQUENCE     �   CREATE SEQUENCE torpedo."HitTable_HitTableID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE torpedo."HitTable_HitTableID_seq";
       torpedo          postgres    false    203    6            �           0    0    HitTable_HitTableID_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE torpedo."HitTable_HitTableID_seq" OWNED BY torpedo."HitTable"."HitTableID";
          torpedo          postgres    false    202            �            1259    16532    Save    TABLE     �   CREATE TABLE torpedo."Save" (
    "SaveID" integer NOT NULL,
    "CreatedAt" timestamp without time zone,
    "SaveName" character varying(30)
);
    DROP TABLE torpedo."Save";
       torpedo         heap    postgres    false    6            �            1259    16530    Save_SaveID_seq    SEQUENCE     �   CREATE SEQUENCE torpedo."Save_SaveID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE torpedo."Save_SaveID_seq";
       torpedo          postgres    false    205    6            �           0    0    Save_SaveID_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE torpedo."Save_SaveID_seq" OWNED BY torpedo."Save"."SaveID";
          torpedo          postgres    false    204            �            1259    16540    Ship    TABLE     �   CREATE TABLE torpedo."Ship" (
    "ShipID" integer NOT NULL,
    "SaveID" integer,
    "VerPosition" smallint,
    "HorPosition" smallint,
    "Arrangement" character(1),
    "Length" smallint,
    "Owner" character varying(6)
);
    DROP TABLE torpedo."Ship";
       torpedo         heap    postgres    false    6            �            1259    16538    Ship_ShipID_seq    SEQUENCE     �   CREATE SEQUENCE torpedo."Ship_ShipID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE torpedo."Ship_ShipID_seq";
       torpedo          postgres    false    6    207            �           0    0    Ship_ShipID_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE torpedo."Ship_ShipID_seq" OWNED BY torpedo."Ship"."ShipID";
          torpedo          postgres    false    206            ;           2604    16527    HitTable HitTableID    DEFAULT     �   ALTER TABLE ONLY torpedo."HitTable" ALTER COLUMN "HitTableID" SET DEFAULT nextval('torpedo."HitTable_HitTableID_seq"'::regclass);
 G   ALTER TABLE torpedo."HitTable" ALTER COLUMN "HitTableID" DROP DEFAULT;
       torpedo          postgres    false    202    203    203            <           2604    16535    Save SaveID    DEFAULT     r   ALTER TABLE ONLY torpedo."Save" ALTER COLUMN "SaveID" SET DEFAULT nextval('torpedo."Save_SaveID_seq"'::regclass);
 ?   ALTER TABLE torpedo."Save" ALTER COLUMN "SaveID" DROP DEFAULT;
       torpedo          postgres    false    205    204    205            =           2604    16543    Ship ShipID    DEFAULT     r   ALTER TABLE ONLY torpedo."Ship" ALTER COLUMN "ShipID" SET DEFAULT nextval('torpedo."Ship_ShipID_seq"'::regclass);
 ?   ALTER TABLE torpedo."Ship" ALTER COLUMN "ShipID" DROP DEFAULT;
       torpedo          postgres    false    206    207    207            �          0    16524    HitTable 
   TABLE DATA           d   COPY torpedo."HitTable" ("HitTableID", "SaveID", "VerPosition", "HorPosition", "Owner") FROM stdin;
    torpedo          postgres    false    203   Q       �          0    16532    Save 
   TABLE DATA           D   COPY torpedo."Save" ("SaveID", "CreatedAt", "SaveName") FROM stdin;
    torpedo          postgres    false    205   �        �          0    16540    Ship 
   TABLE DATA           u   COPY torpedo."Ship" ("ShipID", "SaveID", "VerPosition", "HorPosition", "Arrangement", "Length", "Owner") FROM stdin;
    torpedo          postgres    false    207   {        �           0    0    HitTable_HitTableID_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('torpedo."HitTable_HitTableID_seq"', 30, true);
          torpedo          postgres    false    202            �           0    0    Save_SaveID_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('torpedo."Save_SaveID_seq"', 6, true);
          torpedo          postgres    false    204            �           0    0    Ship_ShipID_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('torpedo."Ship_ShipID_seq"', 108, true);
          torpedo          postgres    false    206            ?           2606    16529    HitTable HitTable_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY torpedo."HitTable"
    ADD CONSTRAINT "HitTable_pkey" PRIMARY KEY ("HitTableID");
 E   ALTER TABLE ONLY torpedo."HitTable" DROP CONSTRAINT "HitTable_pkey";
       torpedo            postgres    false    203            A           2606    16537    Save Save_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY torpedo."Save"
    ADD CONSTRAINT "Save_pkey" PRIMARY KEY ("SaveID");
 =   ALTER TABLE ONLY torpedo."Save" DROP CONSTRAINT "Save_pkey";
       torpedo            postgres    false    205            C           2606    16545    Ship Ship_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY torpedo."Ship"
    ADD CONSTRAINT "Ship_pkey" PRIMARY KEY ("ShipID");
 =   ALTER TABLE ONLY torpedo."Ship" DROP CONSTRAINT "Ship_pkey";
       torpedo            postgres    false    207            D           2606    16546    HitTable HitTable_SaveID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY torpedo."HitTable"
    ADD CONSTRAINT "HitTable_SaveID_fkey" FOREIGN KEY ("SaveID") REFERENCES torpedo."Save"("SaveID");
 L   ALTER TABLE ONLY torpedo."HitTable" DROP CONSTRAINT "HitTable_SaveID_fkey";
       torpedo          postgres    false    205    2881    203            E           2606    16551    Ship Ship_SaveID_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY torpedo."Ship"
    ADD CONSTRAINT "Ship_SaveID_fkey" FOREIGN KEY ("SaveID") REFERENCES torpedo."Save"("SaveID");
 D   ALTER TABLE ONLY torpedo."Ship" DROP CONSTRAINT "Ship_SaveID_fkey";
       torpedo          postgres    false    2881    205    207           