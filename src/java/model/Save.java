/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.sql.Timestamp;
/**
 *
 * @author Win10
 */
@Entity
@Table(name = "Save")
@XmlRootElement
public class Save implements Serializable {



    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SaveID")
    private Integer saveID;
    @Column(name = "CreatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Size(max = 30)
    @Column(name = "SaveName")
    private String saveName;
    @OneToMany(mappedBy = "saveID")
    private Collection<HitTable> hitTableCollection;
    @OneToMany(mappedBy = "saveID")
    private Collection<Ship> shipCollection;

    
    
    public Save() {
    }
    public Save(Date createdAt, String saveName) {
        this.createdAt = createdAt;
        this.saveName = saveName;
    }
    public Save(Integer saveID) {
        this.saveID = saveID;
    }

    public Integer getSaveID() {
        return saveID;
    }

    public void setSaveID(Integer saveID) {
        this.saveID = saveID;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getSaveName() {
        return saveName;
    }

    public void setSaveName(String saveName) {
        this.saveName = saveName;
    }

    @XmlTransient
    public Collection<HitTable> getHitTableCollection() {
        return hitTableCollection;
    }

    public void setHitTableCollection(Collection<HitTable> hitTableCollection) {
        this.hitTableCollection = hitTableCollection;
    }

    @XmlTransient
    public Collection<Ship> getShipCollection() {
        return shipCollection;
    }

    public void setShipCollection(Collection<Ship> shipCollection) {
        this.shipCollection = shipCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (saveID != null ? saveID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Save)) {
            return false;
        }
        Save other = (Save) object;
        if ((this.saveID == null && other.saveID != null) || (this.saveID != null && !this.saveID.equals(other.saveID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Save[ saveID=" + saveID + " ]";
    }

    
}
