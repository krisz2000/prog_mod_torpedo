package model;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class Game {
    
    public ArrayList<Ship> playerShips;
    public ArrayList<Ship> botShips;
    public int[][] playerMap = new int[10][10];
    public int[][] botMap = new int[10][10];
    
    public Game(){
        this.playerShips = new ArrayList<Ship>();
        this.botShips = new ArrayList<Ship>();
        this.playerMap = new int[10][10];
        this.botMap = new int[10][10];
    }

    public Game(ArrayList<Ship> playerShips, ArrayList<Ship> botShips) {
        this.playerShips = playerShips;
        this.botShips = botShips;
        this.playerMap = new int[10][10];
        this.botMap = new int[10][10];
        for(int i = 0; i < playerShips.size(); i++){
            if(playerShips.get(i).arrangement == 'h'){
                for(int j = 0;j<playerShips.get(i).length;j++){
                    try{
                        playerMap[playerShips.get(i).horizontalPos + j][playerShips.get(i).verticalPos] = 1;
                    }
                    catch(Exception ex){
                        //TODO
                    }
                }
            }
            if(playerShips.get(i).arrangement == 'v'){
                for(int j = 0;j<playerShips.get(i).length;j++){
                    try{
                        playerMap[playerShips.get(i).horizontalPos][playerShips.get(i).verticalPos + j] = 1;
                    }
                    catch(Exception ex){
                        //TODO
                    }
                }
            }
        }
        for(int i = 0; i < botShips.size(); i++){
            if(botShips.get(i).arrangement == 'h'){
                for(int j = 0;j<botShips.get(i).length;j++){
                    try{
                        botMap[botShips.get(i).horizontalPos + j][botShips.get(i).verticalPos] = 1;
                    }
                    catch(Exception ex){
                        //TODO
                    }
                }
            }
            if(botShips.get(i).arrangement == 'v'){
                for(int j = 0;j<botShips.get(i).length;j++){
                    try{
                        botMap[botShips.get(i).horizontalPos][botShips.get(i).verticalPos + j] = 1;
                    }
                    catch(Exception ex){
                        //TODO
                    }
                }
            }
        }
        
        
    }

    public JSONArray sendMaps() {
        JSONArray array = new JSONArray();
        JSONObject jsonplayer = new JSONObject();
        JSONObject jsonbot = new JSONObject();
        String playerMapString = "";
        String botMapString = "";
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10;j++){
                playerMapString += this.playerMap[i][j];
            }
        }
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10;j++){
                botMapString += this.botMap[i][j];
            }
        }
        jsonplayer.put("player", playerMapString);
        jsonbot.put("bot", botMapString);
        array.put(playerMapString);
        array.put(botMapString);
        return array;
    }
    
    
}

