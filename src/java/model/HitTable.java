/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Win10
 */
@Entity
@Table(name = "HitTable")
@XmlRootElement
public class HitTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "HitTableID")
    private Integer hitTableID;
    @Column(name = "VerPosition")
    private Integer verticalPos;
    @Column(name = "HorPosition")
    private Integer horizontalPos;
    @Size(max = 6)
    @Column(name = "Owner")
    private String owner;
    @JoinColumn(name = "SaveID", referencedColumnName = "SaveID")
    @ManyToOne
    private Save saveID;

    
    
    
    public HitTable(){
        
    }
    public HitTable(Integer verticalPos, Integer horizontalPos, String owner) {
        this.verticalPos = verticalPos;
        this.horizontalPos = horizontalPos;
        this.owner = owner;
    }

    public Integer getVerticalPos() {
        return verticalPos;
    }

    public Integer getHorizontalPos() {
        return horizontalPos;
    }

    public String getOwner() {
        return owner;
    }

    
}
