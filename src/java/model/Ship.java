/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Win10
 */
@Entity
@Table(name = "Ship")
@XmlRootElement
public class Ship implements Serializable {

    @Column(name = "VerPosition")
    public Integer verticalPos;
    @Column(name = "HorPosition")
    public Integer horizontalPos;
    @Column(name = "Length")
    public Integer length;

    public static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ShipID")
    public Integer shipID;
    @Column(name = "Arrangement")
    public Character arrangement;
    @Size(max = 6)
    @Column(name = "Owner")
    public String owner;
    @JoinColumn(name = "SaveID", referencedColumnName = "SaveID")
    @ManyToOne
    public Save saveID;
    public Boolean sunk;

    public Ship() {
    }
    
    public Ship(Integer verticalPos, Integer horizontalPos, Integer length, char arrangement) {
        this.verticalPos = verticalPos;
        this.horizontalPos = horizontalPos;
        this.length = length;
        this.arrangement = arrangement;
        this.sunk = false;
    }

    public Ship(Integer verticalPos, Integer horizontalPos, Integer length, char arrangement, Boolean sunk) {
        this.verticalPos = verticalPos;
        this.horizontalPos = horizontalPos;
        this.length = length;
        this.arrangement = arrangement;
        this.sunk = sunk;
    }

    public Integer getVerticalPos() {
        return verticalPos;
    }

    public Integer getHorizontalPos() {
        return horizontalPos;
    }

    public Integer getLength() {
        return length;
    }

    public Character getArrangement() {
        return arrangement;
    }
    
    

    
}
