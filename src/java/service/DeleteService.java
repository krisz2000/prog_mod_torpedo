/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import model.Game;
import model.Ship;

/**
 *
 * @author Win10
 */
public class DeleteService {
    
    public Boolean deleteGame(Integer id){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("Prog_Mod_TorpedoPU");
        EntityManager manager = factory.createEntityManager();
        try{
            manager.getTransaction().begin();
            
            StoredProcedureQuery deleteSave = manager.createStoredProcedureQuery("torpedo.delete_save");
            deleteSave.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
            deleteSave.setParameter(0, id);
            deleteSave.execute();
        }
        catch(Exception e){
            System.out.println(e);
            return Boolean.FALSE;
        }
        finally{
            manager.getTransaction().commit();
            manager.close();
            factory.close();
        }
        return Boolean.TRUE;
    }
}
