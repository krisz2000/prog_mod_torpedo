/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import model.Game;
import model.HitTable;
import model.Save;
import model.Ship;
import org.json.JSONArray;
/**
 *
 * @author Win10
 */
public class LoadService {
    
    public Game loadGame(Integer id){
        Game newGame = null;
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("Prog_Mod_TorpedoPU");
        EntityManager manager = factory.createEntityManager();
        try{
            manager.getTransaction().begin();
            
            StoredProcedureQuery getShips = manager.createStoredProcedureQuery("torpedo.get_ships");
            getShips.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
            getShips.setParameter(0, id);
            getShips.execute();
            List<Object[]> ships = getShips.getResultList();
            ArrayList<Ship> playerShips = new ArrayList<>();
            ArrayList<Ship> botShips = new ArrayList<>();
            for(Object[] ship : ships){
                Ship newShip = new Ship((Integer)ship[0], (Integer)ship[1], (Integer)ship[3], ((String)ship[2]).charAt(0));
                if("player".equals((String)ship[4])){
                    newShip.owner = "player";
                    playerShips.add(newShip);
                }else{
                    newShip.owner = "bot";
                    botShips.add(newShip);
                }
            }
            newGame = new Game(playerShips, botShips);
            
            StoredProcedureQuery getHits = manager.createStoredProcedureQuery("torpedo.get_hits");
            getHits.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
            getHits.setParameter(0, id);
            getHits.execute();
            List<Object[]> hits = getHits.getResultList();
            for(Object[] hit : hits){
                if("player".equals((String)hit[2])){
                    Integer verticalPos = (Integer)hit[0];
                    Integer horizontalPos = (Integer)hit[1];
                    newGame.playerMap[verticalPos][horizontalPos] += 2;
                }else{
                    Integer verticalPos = (Integer)hit[0];
                    Integer horizontalPos = (Integer)hit[1];
                    newGame.botMap[verticalPos][horizontalPos] += 2;
                }
            }
            System.out.println("");
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
        finally{
            manager.getTransaction().commit();
            manager.close();
            factory.close();
        }
        GameService.onGoingGame = newGame;
        return newGame;
    }
    public JSONArray getAllGames(){
        JSONArray jsona = new JSONArray();
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("Prog_Mod_TorpedoPU");
        EntityManager manager = factory.createEntityManager();
        try{
            manager.getTransaction().begin();
            StoredProcedureQuery getSaves = manager.createStoredProcedureQuery("torpedo.get_saves");
            getSaves.execute();
            List<Object[]> results = getSaves.getResultList();
            for(Object[] result : results){
                jsona = jsona.put(result);
            }
            Integer id = (Integer)results.get(0)[0];
            System.out.println("");
            //int saveID = (Integer) result.get(0)[0];
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
        finally{
            manager.getTransaction().commit();
            manager.close();
            factory.close();
        }
        return jsona;
    }
}
