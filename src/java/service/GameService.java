package service;


import java.util.ArrayList;
import java.util.Random;
import model.Game;
import model.Ship;

public class GameService {
    
    static Game onGoingGame = new Game();

    public Game newGame(){
        onGoingGame = new Game();
        Random rnd = new Random();
        ArrayList<Ship> playerShips = new ArrayList<>();
        ArrayList<Ship> botShips = new ArrayList<>();
        int[] initialLengths = {2, 2, 2, 3, 3, 3, 4, 4, 5};
        for(int i = initialLengths.length - 1; i>=0; i--){
//fill the player's
            char arrangement;            
            int verPos = rnd.nextInt(10);
            int horPos = rnd.nextInt(10);
            if(rnd.nextInt(2) == 1)arrangement = 'h';
                else arrangement = 'v';
            Ship ship = new Ship(verPos, horPos, initialLengths[i], arrangement);
            if(ship.arrangement == 'v'){
                if(ship.verticalPos + ship.length > 9)i++;
        //make sure, its on the map
                else{
                    if(!posFinder(ship, onGoingGame.playerMap)){
                        playerShips.add(ship);
                        shipFill(onGoingGame.playerMap,ship);
                    }
        //make sure, no one's near
                    else i++;
                }
            }
            else{
                if(ship.horizontalPos + ship.length > 9)i++;
        //make sure, its on the map
                else{
                    if(!posFinder(ship, onGoingGame.playerMap)){
                        playerShips.add(ship);
                        shipFill(onGoingGame.playerMap,ship);
                    }
        //make sure, no one's near
                    else i++;
                }
            }
        }
        for(int i = initialLengths.length - 1; i >= 0; i--){
//fill the bot's
            char arrangement;            
            int verPos = rnd.nextInt(10);
            int horPos = rnd.nextInt(10);
            if(rnd.nextInt(2) == 1)arrangement = 'h';
                else arrangement = 'v';
            Ship ship = new Ship(verPos, horPos, initialLengths[i], arrangement);
            if(ship.arrangement == 'v'){
                if(ship.verticalPos + ship.length > 9)i++;
        //make sure, its on the map
                else{
                    if(!posFinder(ship, onGoingGame.botMap)){
                        botShips.add(ship);
                        shipFill(onGoingGame.botMap,ship);
                    }
        //make sure, no one's near
                    else i++;
                }
            }
            else{
                if(ship.horizontalPos + ship.length > 9)i++;
        //make sure, its on the map
                else{
                    if(!posFinder(ship, onGoingGame.botMap)){
                        botShips.add(ship);
                        shipFill(onGoingGame.botMap,ship);
                    }
        //make sure, no one's near
                    else i++;
                }
            }
        }
        onGoingGame = new Game(playerShips, botShips);
        shipFiller();
        emptyFiller();
        return onGoingGame;
    }
    
    public static Game returnGame(){
        return onGoingGame;
    }
    
    public Boolean posFinder(Ship ship, int[][] map){//checks, whether this ship is close to another
        Boolean taken = false;
        if(ship.arrangement == 'v'){
            for(int hor = -1; hor <= 1; hor++){
                for(int ver = -1; ver<= ship.length; ver++){
                    try{
            //this position might be out of the array
                        if(map[ship.horizontalPos + hor][ship.verticalPos + ver] == 1){
                            taken = true;
                            return taken;
                        }
                    }
                    catch(Exception ex){
                        
                    }
                }
            }
        }
        else{
            for(int ver = -1; ver <= 1; ver++){
                for(int hor = -1; hor<= ship.length; hor++){
                    try{
            //this position might be out of the array
                        if(map[ship.horizontalPos + hor][ship.verticalPos + ver] == 1){
                            taken = true;
                            return taken;
                        }
                    }
                    catch(Exception ex){
                        
                    }
                }
            }
        }
        return taken;
    }//checks, whether this ship is close to another
    
    public static void shipFill(int[][] map, Ship ship){//fills the places of the ships
        if(ship.arrangement == 'v'){
            for(int i=0;i<ship.length;i++){
                map[ship.horizontalPos][ship.verticalPos + i] = 1;
            }
        }
        else{
            for(int i=0;i<ship.length;i++){
                map[ship.horizontalPos + i][ship.verticalPos] = 1;
            }
        }
    }

    public void shipFiller(){//fills the places of the ships
        for(int i=0;i<onGoingGame.botShips.size();i++){
            if(onGoingGame.botShips.get(i).arrangement == 'h'){
                for(int j = 0; j<onGoingGame.botShips.get(i).length;j++){
                    onGoingGame.botMap[onGoingGame.botShips.get(i).horizontalPos+j][onGoingGame.botShips.get(i).verticalPos] = 1;
                }
            }
            else{
                for(int j = 0; j<onGoingGame.botShips.get(i).length;j++){
                    onGoingGame.botMap[onGoingGame.botShips.get(i).horizontalPos][onGoingGame.botShips.get(i).verticalPos + j] = 1;
                }
            }
        }
        for(int i=0;i<onGoingGame.playerShips.size();i++){
            if(onGoingGame.playerShips.get(i).arrangement == 'h'){
                for(int j = 0; j<onGoingGame.playerShips.get(i).length;j++){
                    onGoingGame.playerMap[onGoingGame.playerShips.get(i).horizontalPos + j][onGoingGame.playerShips.get(i).verticalPos] = 1;
                }
            }
            else{
                for(int j = 0; j<onGoingGame.playerShips.get(i).length;j++){
                    onGoingGame.playerMap[onGoingGame.playerShips.get(i).horizontalPos][onGoingGame.playerShips.get(i).verticalPos + j] = 1;
                }
            }
        }
    }
    
    public void emptyFiller(){//fills the empty spaces with 0s
        for(int i=0; i<10; i++){
            for(int j = 0; j<10; j++){
                if(onGoingGame.botMap[i][j] != 1)onGoingGame.botMap[i][j]=0;
                if(onGoingGame.playerMap[i][j] != 1)onGoingGame.playerMap[i][j]=0;
            }
        }
    }
    
    public static String hitDeploy(Integer x, Integer y){//deploy the the hit on the given area and return the value
        onGoingGame.botMap[x][y] +=2;
        Integer hit = onGoingGame.botMap[x][y];
        return hit.toString();
    }
    
    public static String botServiceTarget(){
//find the a possible hit to an already hit spot, else hit randomly
        Integer xtarget;
        Integer ytarget;
        String target;
        for(int x = 0; x<10; x++){
            for(int y = 0; y<10; y++){
                if(onGoingGame.playerMap[x][y] == 3){
                    Boolean shipNext = isShipNext(x,y);
                    Boolean hitNext = isHitNext(x,y);
                    if(shipNext && hitNext){//if theres a ship nearby and a hit as well, add another hit
                        return hitShipNext(x,y);
                    }
                    else if(shipNext){//if theres a ship nearby, but only one hit, pick randomly
                        return hitAround(x,y);
                    }
                    else if(hitNext){//if theres only a hit without a ship, go off                        
                        String result = missNext(x,y);                        
                        if(result.length() == 2)return result;
                    }                      
                }
            }    
        }
        Random rnd = new Random();
        do{//otherwise hit a random free spot
            xtarget = rnd.nextInt(10);
            ytarget = rnd.nextInt(10);
        }while(onGoingGame.playerMap[xtarget][ytarget] > 1);
        onGoingGame.playerMap[xtarget][ytarget] += 2;
        target = xtarget.toString()+ytarget.toString();
        return target;
    }
    
    public static String botServiceValue(String target){
        String x = Character.toString(target.charAt(0));
        String y = Character.toString(target.charAt(1));
        Integer value = onGoingGame.playerMap[Integer.parseInt(x)][Integer.parseInt(y)];
        return value.toString();        
    }
    
    public static Boolean isShipNext(Integer x, Integer y){//is a ship next to the previous hit
        try{
            if(onGoingGame.playerMap[x-1][y] == 1){
                return true;
            }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x+1][y] == 1){
            return true;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x][y-1] == 1){
            return true;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x][y+1] == 1){
            return true;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        return false;
    }
    
    public static Boolean isHitNext(Integer x, Integer y){//is a hit next to the previous hit
        try{
            if(onGoingGame.playerMap[x-1][y] == 3){
                return true;
            }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x+1][y] == 3){
            return true;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x][y-1] == 3){
            return true;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x][y+1] == 3){
            return true;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        return false;
    }
    
    public static String hitShipNext(Integer x, Integer y){//hit the ship next to the previous hit
        Integer xtarget;
        Integer ytarget;
        String target;
        try{if(onGoingGame.playerMap[x-1][y] == 1){
            xtarget = x-1;
            ytarget = y;
            onGoingGame.playerMap[xtarget][ytarget] += 2;
            target = xtarget.toString()+ytarget.toString();
            return target;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x+1][y] == 1){
            xtarget = x+1;
            ytarget = y;
            onGoingGame.playerMap[xtarget][ytarget] += 2;
            target = xtarget.toString()+ytarget.toString();
            return target;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x][y-1] == 1){
            xtarget = x;
            ytarget = y-1;
            onGoingGame.playerMap[xtarget][ytarget] += 2;
            target = xtarget.toString()+ytarget.toString();
            return target;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        try{if(onGoingGame.playerMap[x][y+1] == 1){
            xtarget = x;
            ytarget = y+1;
            onGoingGame.playerMap[xtarget][ytarget] += 2;
            target = xtarget.toString()+ytarget.toString();
            return target;
        }}
        catch(Exception ex){
            System.out.println(ex);
        }
        return "00";
    }
    
    public static String hitAround(Integer x, Integer y){//hit a free spot randomly        
        Random rnd = new Random();
        do{
            Integer caseNum = rnd.nextInt(4);
            if(caseNum == 0 && x+1 < 10 && onGoingGame.playerMap[x+1][y] < 2){                
                x++;
                onGoingGame.playerMap[x][y] += 2;
                return x.toString()+y.toString();
            }
            else if(caseNum == 1 && y+1 < 10 && onGoingGame.playerMap[x][y+1] < 2){
                y++;
                onGoingGame.playerMap[x][y] += 2;
                return x.toString()+y.toString();
            }
            else if(caseNum == 2 && x-1 >= 0 && onGoingGame.playerMap[x-1][y] < 2){
                x--;
                onGoingGame.playerMap[x][y] += 2;
                return x.toString()+y.toString();
            }
            else if(caseNum == 3 && y-1 >= 0 && onGoingGame.playerMap[x][y-1] < 2){
                y--;
                onGoingGame.playerMap[x][y] += 2;
                return x.toString()+y.toString();
            }
        }while(true);
    }
    
    public static String missNext(Integer x, Integer y){//miss next to the previous hit
        if(x+1 < 10 && x > 0 && onGoingGame.playerMap[x+1][y] == 3 && onGoingGame.playerMap[x-1][y] < 2){
            onGoingGame.playerMap[x-1][y] += 2;
            x--;
            return x.toString()+y.toString();
        }
        if(x-1 >= 0 && x < 9 && onGoingGame.playerMap[x-1][y] == 3 && onGoingGame.playerMap[x+1][y] < 2){
            onGoingGame.playerMap[x+1][y] += 2;
            x++;
            return x.toString()+y.toString();
        }
        if(y-1 >= 0 && y < 9 && onGoingGame.playerMap[x][y-1] == 3 && onGoingGame.playerMap[x][y+1] < 2){
            onGoingGame.playerMap[x][y+1] += 2;
            y++;
            return x.toString()+y.toString();
        }
        if(y+1 < 10 && y > 0 && onGoingGame.playerMap[x][y+1] == 3 && onGoingGame.playerMap[x][y-1] < 2){
            onGoingGame.playerMap[x][y-1] += 2;
            y--;
            return x.toString()+y.toString();
        }
        return "no result";
    }
    
    public static Boolean playerWon(){
        for(int x = 0; x < 10; x++){
            for(int y = 0; y < 10; y++){
                if(onGoingGame.botMap[x][y] == 1)return false;
            }
        }
        return true;
    }
    
    public static Boolean botWon(){
        for(int x = 0; x < 10; x++){
            for(int y = 0; y < 10; y++){
                if(onGoingGame.playerMap[x][y] == 1)return false;
            }
        }
        return true;
    }
    
    
    


}


    

