/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import controller.GameController;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import model.Save;
import model.Ship;
import service.GameService;
import model.Game;
import model.HitTable;
/**
 *
 * @author Win10
 */
public class SaveService {
    
    
    public Boolean saveGame(String name){
        Game game = GameService.returnGame();
        if(!game.playerShips.isEmpty()){
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("Prog_Mod_TorpedoPU");
            EntityManager manager = factory.createEntityManager();
            try{
                manager.getTransaction().begin();
                Date currDate = new Date(System.currentTimeMillis());
                Save save = new Save(new Timestamp(currDate.getTime()), name);
                int SaveID = createSave(save, manager);
                StoredProcedureQuery createShip = manager.createStoredProcedureQuery("torpedo.create_ship");
                createShip.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
                createShip.registerStoredProcedureParameter(1, Short.class, ParameterMode.IN);
                createShip.registerStoredProcedureParameter(2, Short.class, ParameterMode.IN);
                createShip.registerStoredProcedureParameter(3, Character.class, ParameterMode.IN);
                createShip.registerStoredProcedureParameter(4, Short.class, ParameterMode.IN);
                createShip.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
                for(Ship ship : game.playerShips){
                    saveShip(SaveID, ship, createShip, "player");
                }
                for(Ship ship : game.botShips){
                    saveShip(SaveID, ship, createShip, "bot");
                }
                StoredProcedureQuery createHit = manager.createStoredProcedureQuery("torpedo.create_hit");
                createHit.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
                createHit.registerStoredProcedureParameter(1, Short.class, ParameterMode.IN);
                createHit.registerStoredProcedureParameter(2, Short.class, ParameterMode.IN);
                createHit.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
                for (int i = 0; i < game.playerMap.length; i++) {
                    for (int j = 0; j < game.playerMap[i].length; j++) {
                        if(game.playerMap[i][j] >= 2){
                            HitTable hit = new HitTable(i, j, "player");
                            saveHit(SaveID, hit, createHit);
                        }
                    }
                }
                for (int i = 0; i < game.botMap.length; i++) {
                    for (int j = 0; j < game.botMap[i].length; j++) {
                        if(game.botMap[i][j] >= 2){
                            HitTable hit = new HitTable(i, j, "bot");
                            saveHit(SaveID, hit, createHit);
                        }
                    }
                }
            }
            catch(Exception e){
                System.out.println(e);
                return Boolean.FALSE;
            }
            finally{
                manager.getTransaction().commit();
                manager.close();
                factory.close();
            }
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    
    public int createSave(Save save, EntityManager manager){
        StoredProcedureQuery spq = manager.createStoredProcedureQuery("torpedo.create_save");
        spq.registerStoredProcedureParameter(0, String.class, ParameterMode.IN);
        spq.setParameter(0, save.getSaveName());
        spq.execute();
        Object[] result = (Object[])spq.getSingleResult();
        int saveID = (Integer) result[0];
        return saveID;
    }
    
    public void saveShip(int SaveID, Ship ship, StoredProcedureQuery createShip, String owner){
        createShip.setParameter(0, SaveID);
        createShip.setParameter(1, ship.getVerticalPos().shortValue());
        createShip.setParameter(2, ship.getHorizontalPos().shortValue());
        createShip.setParameter(3, ship.getArrangement());
        createShip.setParameter(4, ship.getLength().shortValue());
        createShip.setParameter(5, owner);
        createShip.execute();
    }
    
    public void saveHit(int SaveID, HitTable hit, StoredProcedureQuery createHit){
        createHit.setParameter(0, SaveID);
        createHit.setParameter(1, hit.getVerticalPos().shortValue());
        createHit.setParameter(2, hit.getHorizontalPos().shortValue());
        createHit.setParameter(3, hit.getOwner());
        createHit.execute();
    }
}
