/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Game;
import org.json.JSONArray;
import org.json.JSONObject;
import service.GameService;

/**
 *
 * @author Roli
 */
@WebServlet(name = "NewServlet", urlPatterns = {"/NewServlet"})
public class GameController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
        response.setContentType("application/json");//formerly "text/html;charset=UTF-8"
        try{
            PrintWriter out = response.getWriter();
            
            GameService gs = new GameService();
            
            
            if(request.getParameter("task") != null){
                if(request.getParameter("task").equals("newGame")){
                    try{                                                 
                        Game newGame = gs.newGame();
                        out.write(newGame.sendMaps().toString());
                        //out.write("{\"result\":\"true\"}");
                    }
                    catch(Exception ex){
                        System.out.println(ex.toString());
                    }
                    
                }
                if(request.getParameter("task").equals("hit")){
                    try{                        
                        String target = request.getParameter("target");
                        String x = Character.toString(target.charAt(0));
                        String y = Character.toString(target.charAt(1));
                        
                        
                        
                        JSONArray jsona = new JSONArray();
                        JSONObject bothit = new JSONObject();//0-3
                        JSONObject playerposition = new JSONObject();//position together
                        JSONObject playerhit = new JSONObject();//0-3
                        JSONObject wintext = new JSONObject();//false-true
                        
                        String bothitString = GameService.hitDeploy(Integer.parseInt(x), Integer.parseInt(y));
                    //TODO
                        String playerHitTarget = GameService.botServiceTarget();//example: 34
                        String playerHitValue = GameService.botServiceValue(playerHitTarget);
                    //TODO over
                        
                        bothit.put("bothit", bothitString);
                        playerposition.put("playertarget", playerHitTarget);
                        playerhit.put("playerhit", playerHitValue);
                        
                        jsona.put(bothit);
                        jsona.put(playerposition);
                        jsona.put(playerhit);
                        
                        if(GameService.playerWon()){
                            wintext.put("winner", "player");
                            jsona.put(wintext);
                        }
                        else if(GameService.botWon()){
                            wintext.put("winner", "bot");
                            jsona.put(wintext);
                        }
                        
                        out.write(jsona.toString());
                        
                        
                        
                        
                    }
                    catch(Exception ex){
                        System.out.println(ex.toString());
                    }
                }
            }
            else{
                System.out.println("Something went wrong");
            }
            
            
            
            
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
