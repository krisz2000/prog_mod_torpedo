#!/bin/sh

asadmin start-domain
asadmin undeploy --user admin -W /usr/pass app
asadmin deploy --user admin -W /usr/pass --contextroot prog_mod_torpedo /app.war
asadmin stop-domain
asadmin start-domain --verbose
