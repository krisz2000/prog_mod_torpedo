FROM glassfish:latest

EXPOSE 4848 8080 8181 9009

COPY /dist/*.war /app.war
COPY /glassfish/start.sh /usr/start.sh
COPY /glassfish/pass /usr/pass
ENTRYPOINT ["/usr/start.sh"]
