package service;

import model.Game;
import org.junit.Test;
import static org.junit.Assert.*;
import static service.GameService.onGoingGame;

public class GameServiceTest {
    
    public GameServiceTest() {
    }

    /**
     * Test of newGame method, of class GameService.
     */
    @Test
    public void testNewGame() {
//check whether both haves have same amount of ship units as predetermined
        GameService service = new GameService();
        Game testGame = service.newGame();
        Integer expectation = 28;
        Integer sumP = 0, sumB = 0;
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++){
                sumP += testGame.playerMap[i][j];
                sumB += testGame.botMap[i][j];
            }
        }
        sumP -= expectation;
        sumB -= expectation;
        assertEquals(sumP, sumB);        
    }

    /**
     * Test of returnGame method, of class GameService.
     */
    @Test
    public void testReturnGame() {
//test whether the new game is the same as the returning game
        GameService service = new GameService();        
        assertEquals(service.newGame(), service.returnGame());
    }


    /**
     * Test of shipFill method, of class GameService.
     */
    @Test
    public void testShipFill() {
//test that a ship is where its supposed to be
        GameService service = new GameService();
        service.newGame();
        Integer expectedValue = 1;
        Integer posValue = GameService.onGoingGame.botMap[GameService.onGoingGame.botShips.get(0).horizontalPos][GameService.onGoingGame.botShips.get(0).verticalPos];
        assertEquals(posValue, expectedValue);
    }

    /**
     * Test of shipFiller method, of class GameService.
     */
    @Test
    public void testShipFiller() {
//test that a ship is where its supposed to be
        GameService service = new GameService();
        service.newGame();
        Integer expectedValue = 1;
        Integer posValue = GameService.onGoingGame.botMap[GameService.onGoingGame.botShips.get(0).horizontalPos][GameService.onGoingGame.botShips.get(0).verticalPos];
        assertEquals(posValue, expectedValue);
    }

    /**
     * Test of emptyFiller method, of class GameService.
     */
    @Test
    public void testEmptyFiller() {
//test that an empty space is where its supposed to be
        GameService service = new GameService();
        service.newGame();
        Integer expectedValue = 0;
        Integer posValue;
//could be out of the map
        try{
            posValue = GameService.onGoingGame.botMap[GameService.onGoingGame.botShips.get(0).horizontalPos + 1][GameService.onGoingGame.botShips.get(0).verticalPos + 1];
        }
        catch(Exception ex){
            posValue = GameService.onGoingGame.botMap[GameService.onGoingGame.botShips.get(0).horizontalPos - 1][GameService.onGoingGame.botShips.get(0).verticalPos - 1];
        }
        assertEquals(posValue, expectedValue);
    }

    /**
     * Test of hitDeploy method, of class GameService.
     */
    @Test
    public void testHitDeploy() {
//test that a hit gets deployed
        GameService service = new GameService();
        service.newGame();
        Integer expectedValue = GameService.onGoingGame.botMap[0][0] + 2;
        GameService.hitDeploy(0, 0);
        Integer result = GameService.onGoingGame.botMap[0][0];
        assertEquals(result, expectedValue);
    }


    /**
     * Test of botServiceValue method, of class GameService.
     */
    @Test
    public void testBotServiceValue() {
        GameService service = new GameService();
        service.newGame();
        String target = "55";
        String x = Character.toString(target.charAt(0));
        String y = Character.toString(target.charAt(1));
        Integer result = onGoingGame.botMap[Integer.parseInt(x)][Integer.parseInt(y)];
        Integer expectedValue = GameService.onGoingGame.botMap[5][5];
        assertEquals(result, expectedValue);
    }
    

    /**
     * Test of playerWon method, of class GameService.
     */
    @Test
    public void testPlayerWon() {
        Boolean expectedValue = true;
        Boolean result = GameService.playerWon();
        for(int x = 0; x < 10; x++){
            for(int y = 0; y < 10; y++){
                if(onGoingGame.botMap[y][x] == 1){
                    expectedValue = false;
                }
            }
        }
        assertEquals(result, expectedValue);
    }

    /**
     * Test of botWon method, of class GameService.
     */
    @Test
    public void testBotWon() {
        Boolean expectedValue = true;
        Boolean result = GameService.botWon();
        for(int x = 0; x < 10; x++){
            for(int y = 0; y < 10; y++){
                if(onGoingGame.playerMap[y][x] == 1){
                    expectedValue = false;
                }
            }
        }
        assertEquals(result, expectedValue);
    }
    
}
